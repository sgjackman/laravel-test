<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
	/**
	 * Allowed for mass assignment
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name',
		'last_name',
		'dob',
		'company_name',
		'position',
		'email',
	];
	public function phone_numbers(){
		return $this->hasMany('App\ContactsPhoneNumbers');
	}
	public function addresses(){
		return $this->hasMany('App\ContactsAddresses');
	}
}
