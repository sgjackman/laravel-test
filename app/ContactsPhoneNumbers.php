<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsPhoneNumbers extends Model
{
	protected $guarded = [];

	public $timestamps = false;
}
