<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp;

class AuthController extends Controller {


	/**
	 *
	 * Handle login:
	 *
	 * We are abstracting our passport client secret here and getting a token / returning error response

	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse|\Psr\Http\Message\StreamInterface
	 */
	public function login( Request $request ) {

		$http = new GuzzleHttp\Client;

		try {

			$response = $http->post( config('services.passport.login_endpoint'), [
				'form_params' => [
					'grant_type'    => 'password',
					'client_id'     => config('services.passport.client_id'),
					'client_secret' => config('services.passport.client_secret'),
					'username'      => $request->username,
					'password'      => $request->password,
				]
			] );

			return $response->getBody();

		} catch ( GuzzleHttp\Exception\BadResponseException $e ) {

			if ( $e->getCode() == 400 ) {
				return response()->json( 'Invalid request. Please enter username and password.', $e->getCode() );
			} else if ( $e->getCode() == 401 ) {
				return response()->json( 'Login details incorrect - please try again.', $e->getCode() );

			}

			return response()->json( 'There was an error on the server.', $e->getCode() );
		}
	}
}
