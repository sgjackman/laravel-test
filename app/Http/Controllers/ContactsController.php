<?php

namespace App\Http\Controllers;

use App\Contacts;

use Illuminate\Http\Request;

class ContactsController extends Controller {
	//
	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function index( Request $request ) {
		$page        = $request->get( 'page' );
		$rowsPerPage = $request->get( 'rowsPerPage' );
		$sortBy      = $request->get( 'sortBy' );
		$searchTerm  = $request->get( 'searchTerm' );

		if ( $sortBy == 'fullName' || empty( $sortBy ) ) {
			$sortBy = 'first_name';
		}


		$order = ( $request->get( 'descending' ) == 'true' ) ? 'desc' : 'asc';


		return Contacts::where( function ( $query ) use ( $searchTerm ) {
			if ( ! empty( $searchTerm ) ) {
				$query->where( 'first_name', 'like', $searchTerm . '%' )
				      ->orWhere( 'last_name', 'like', $searchTerm . '%' )
				      ->orWhere( 'company_name', 'like', $searchTerm . '%' )
				      ->orWhere( 'email', 'like', $searchTerm . '%' );
			}

			return $query;
		} )
		               ->orderBy( $sortBy, $order )
		               ->paginate( $rowsPerPage, [ '*' ], 'page', $page );
	}

	public function update( Request $request, $id ) {
		$request->validate(
			[
				'first_name'   => 'required',
				'last_name'    => 'required',
				'company_name' => 'required',
				'dob'          => 'required',
				'position'     => 'required',
				'email'        => 'required|email|unique:contacts,email,' . $id
			]
		);
		$contact = Contacts::find( $id );
		$contact->update( $request->all() );

		return $contact;
	}

	public function create( Request $request ) {
		$request->validate(
			[
				'first_name'   => 'required',
				'last_name'    => 'required',
				'company_name' => 'required',
				'dob'          => 'required',
				'position'     => 'required',
				'email'        => 'required|email|unique:contacts,email'
			]
		);
		$contact = Contacts::create( $request->all() );

		return $contact;
	}

	public function delete( $id ) {

		$contact = Contacts::find( $id );
		$contact->delete();

		return;
	}

}
