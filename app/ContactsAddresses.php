<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsAddresses extends Model
{
	/**
	 * Allowed for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [];

	public $timestamps = false;
}
