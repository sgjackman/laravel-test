<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1','middleware' => ['auth:api','JsonMiddleware']], function() {
	Route::put('/user', 'LoggedUserController@update');

	// Contacts Routes
	Route::get('/contacts', 'ContactsController@index');
	Route::put('/contact/{id}', 'ContactsController@update');
	Route::post('/contact', 'ContactsController@create');
	Route::delete('/contact/{id}', 'ContactsController@delete');
});

Route::post('/login', 'AuthController@login');