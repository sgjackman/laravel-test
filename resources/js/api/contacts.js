import axios from 'axios'

export default {
    fetch(params) {
        return axios.get('/api/v1/contacts', {params: params})
    },
    update(contact) {
        return axios.put('/api/v1/contact/'+contact.id, {
            'first_name': contact.first_name,
            'last_name': contact.last_name,
            'dob': contact.dob,
            'company_name': contact.company_name,
            'position': contact.position,
            'email': contact.email,
        })
    },
    create(contact) {
        return axios.post('/api/v1/contact', {
            'first_name': contact.first_name,
            'last_name': contact.last_name,
            'dob': contact.dob,
            'company_name': contact.company_name,
            'position': contact.position,
            'email': contact.email,
        })
    },
    delete(contact) {
        return axios.delete('/api/v1/contact/'+contact.id)
    }
}
