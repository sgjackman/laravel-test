import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  selected_contact: {},
  contacts: []
}

export default {
  state,
  getters,
  actions,
  mutations
}
