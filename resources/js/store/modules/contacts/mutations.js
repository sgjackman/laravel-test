import * as types from '../../mutation-types'

export default {
  [ types.SELECTED_CONTACT ] (state, contact) {
    state.selected_contact = contact
  },
  [ types.SET_CONTACTS ] (state, contacts) {
    state.contacts = contacts
  }
}
