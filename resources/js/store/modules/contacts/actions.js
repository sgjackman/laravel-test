import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import contacts from '../../../api/contacts'

export default {
  [ actions.SELECTED_CONTACT ] (context, contact) {
    context.commit(mutations.SELECTED_CONTACT, contact)
  },
  [ actions.FETCH_CONTACTS ] (context, params) {
    return new Promise((resolve, reject) => {
      contacts.fetch(params).then(response => {
        context.commit(mutations.SET_CONTACTS, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [ actions.UPDATE_CONTACT ] (context, contact) {
    return new Promise((resolve, reject) => {
      contacts.update(contact).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [ actions.CREATE_CONTACT ] (context, contact) {
    return new Promise((resolve, reject) => {
      contacts.create(contact).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [ actions.DELETE_CONTACT ] (context, contact) {
    return new Promise((resolve, reject) => {
      contacts.delete(contact).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  }
}
