export default {
  selectedContact: state => state.selected_contact,
  contacts: state => state.contacts
}
