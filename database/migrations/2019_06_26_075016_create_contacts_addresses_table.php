<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_addresses', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('contacts_id')->index();
	        $table->enum('address_type',['Work','Postal','Other'])->default('work');
	        $table->text('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_addresses');
    }
}
