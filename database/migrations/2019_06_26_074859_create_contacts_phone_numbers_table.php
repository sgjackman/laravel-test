<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsPhoneNumbersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts_phone_numbers', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('contacts_id')->index();
			$table->enum('phone_type',['Mobile','Work','Home','Other'])->default('mobile');
			$table->string('phone_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contacts_phone_numbers');
	}
}
