<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$password = Hash::make('password');
		$username = "admin";

		User::create( [
			'name'     => $username,
			'email'    => 'user@test.com',
			'password' => $password
		] );
	}
}
