<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Contacts;
use App\ContactsAddresses;
use App\ContactsPhoneNumbers;

class ContactsSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();

		$limit = 2000;

		$phone_types = ['Mobile','Work','Home','Other'];

		$address_types = ['Work','Postal','Other'];

		$positions = ['Head Honcho','Salesperson','Grunt'];

		for ( $i = 0; $i < $limit; $i ++ ) {
			$new_customer = Contacts::create( [ //,
				'first_name'   => $faker->firstName,
				'last_name'    => $faker->lastName,
				'email'        => $faker->unique()->email,
				'dob'          => $faker->dateTimeBetween( '1971-01-01', '2007-01-01' ),
				'position' => $positions[rand(0,2)],
				'company_name' => $faker->unique()->company,
				'created_at'   => Carbon::now(),
			] );
			// create random number of phone numbers for this contact
			$num = rand( 0, 3 );
			for ( $j = 0; $j <= $num; $j ++ ) {
				$new_phone = new ContactsPhoneNumbers( [ //,
					'phone_number'      => $faker->phoneNumber,
					'phone_type' => $phone_types[$j]
				] );
				$new_customer->phone_numbers()->save( $new_phone );
			}
			$num = rand( 0, 2 );
			for ( $j = 0; $j <= $num; $j ++ ) {
				$new_phone = new ContactsAddresses( [ //,
					'address'      => $faker->address,
					'address_type' => $address_types[$j]
				] );
				$new_customer->phone_numbers()->save( $new_phone );
			}
		}
	}
}
