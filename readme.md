### Installation Instructions

`npm install`

`npm run dev`

Configure your favorite database (mysql, sqlite etc.)

copy .env.example to .env and update the db connection details

`php artisan migrate` to create basic user tables.

This packages requires Laravel Passport. Run 

`php artisan passport:install`

edit .env and update the following

 - PASSPORT_LOGIN_ENDPOINT
 - PASSPORT_CLIENT_ID
 - PASSPORT_CLIENT_SECRET
 
 You'll need to check the oauth_clients table for these values
 
`npm run build`
 
 You should be good to go, unless I've forgotten something which is extremely possible at this hour.

